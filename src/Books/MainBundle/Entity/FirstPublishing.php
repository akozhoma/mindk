<?php

namespace Books\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FirstPublishing
 *
 * @ORM\Entity
 */
class FirstPublishing extends Publishing
{
    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=100)
     */
    private $city;

    /**
     * @var integer
     *
     * @ORM\Column(name="ageAuthor", type="integer")
     */
    private $ageAuthor;

    protected $publishingType;

    /**
     * Set city
     *
     * @param string $city
     * @return FirstPublishing
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set ageAuthor
     *
     * @param integer $ageAuthor
     * @return FirstPublishing
     */
    public function setAgeAuthor($ageAuthor)
    {
        $this->ageAuthor = $ageAuthor;

        return $this;
    }

    /**
     * Get ageAuthor
     *
     * @return integer 
     */
    public function getAgeAuthor()
    {
        return $this->ageAuthor;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get author
     *
     * @return string 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    public function setPublishingType($publishingType)
    {
        $this->publishingType = parent::getPublishingType();

        return $this;
    }

    /**
     * Get publishingType
     *
     * @return integer 
     */
    public function getPublishingType()
    {
        return parent::getPublishingType();
    }
}
