<?php

namespace Books\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Publishing
 *
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="publishingType", type="integer")
 * @ORM\DiscriminatorMap({"0" = "Publishing", "1" = "FirstPublishing", "2" = "SecondPublishing"})
 */
class Publishing
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=150)
     */
    protected $author;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer")
     */
    protected $year;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Publishing
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return Publishing
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return Publishing
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set publishingType
     *
     * @param integer $publishingType
     * @return Publishing
     */
    public function setPublishingType($publishingType)
    {
        $this->publishingType = $publishingType;

        return $this;
    }

    /**
     * Get publishingType
     *
     * @return boolean
     */
    public function getPublishingType()
    {
        return $this->publishingType;
    }
}
