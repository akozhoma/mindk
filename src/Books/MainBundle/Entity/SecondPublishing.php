<?php

namespace Books\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SecondPublishing
 *
 * @ORM\Entity
 */
class SecondPublishing extends Publishing
{
    /**
     * @var integer
     *
     * @ORM\Column(name="countPages", type="integer")
     */
    private $countPages;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=150)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=150)
     */
    private $telephone;

    protected $publishingType;

    /**
     * Set countPages
     *
     * @param integer $countPages
     * @return SecondPublishing
     */
    public function setCountPages($countPages)
    {
        $this->countPages = $countPages;

        return $this;
    }

    /**
     * Get countPages
     *
     * @return integer 
     */
    public function getCountPages()
    {
        return $this->countPages;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return SecondPublishing
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return SecondPublishing
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get author
     *
     * @return string 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    public function setPublishingType($publishingType)
    {
        $this->publishingType = parent::getPublishingType();

        return $this;
    }

    /**
     * Get publishingType
     *
     * @return integer 
     */
    public function getPublishingType()
    {
        return parent::getPublishingType();
    }
}
