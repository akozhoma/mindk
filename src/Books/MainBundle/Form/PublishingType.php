<?php

namespace Books\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Books\MainBundle\Form\FirstPublishingType;
use Books\MainBundle\Form\SecondPublishingType;

class PublishingType extends AbstractType
{
    private $choices;

    private $preferredChoice;

    public function __construct($choices = array(), $preferredChoice = NULL) {
        $this->choices = $choices;
        $this->preferredChoice = $preferredChoice;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'attr' => array(
                    'class' => 'col-lg-8',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-4',
                ),
                'required' => TRUE,
            ))
            ->add('author', null, array(
                'attr' => array(
                    'class' => 'col-lg-8',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-4',
                ),
                'required' => TRUE,
            ))
            ->add('year', null, array(
                'attr' => array(
                    'class' => 'col-lg-8',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-4',
                ),
                'required' => TRUE,
            ));

        if (!empty($this->choices)) {
            $builder->add('choicePublishing', 'choice', array(
                'mapped' => FALSE,
                'choices' => $this->choices,
                'attr' => array(
                    'class' => 'col-lg-8 dropdown-toggle btn choice-publishing',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-4',
                ),
                'empty_value' => 'Choose publishing',
                'required' => FALSE,
                'label' => 'Choice publishing',
            ));
        }

        if (isset($this->choices['first']) || $this->preferredChoice == 'first') {
            $builder->add('firstPublishing', new FirstPublishingType(), array(
                'mapped' => FALSE,
                'attr' => array(
                    'class' => 'hidden first publishing input-lg',
                ),
                'label_attr' => array(
                    'class' => 'hidden',
                ),
            ));
        }

        if (isset($this->choices['second']) || $this->preferredChoice == 'second') {
            $builder->add('secondPublishing', new SecondPublishingType(), array(
                'mapped' => FALSE,
                'attr' => array(
                    'class' => 'hidden second publishing input-lg',
                ),
                'label_attr' => array(
                    'class' => 'hidden',
                ),
            ));
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Books\MainBundle\Entity\Publishing',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'books_mainbundle_publishing';
    }
}
