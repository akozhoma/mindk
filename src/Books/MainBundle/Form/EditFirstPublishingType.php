<?php

namespace Books\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditFirstPublishingType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'attr' => array(
                    'class' => 'col-lg-8',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-4',
                ),
                'required' => TRUE,
            ))
            ->add('author', 'text', array(
                'attr' => array(
                    'class' => 'col-lg-8',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-4',
                ),
                'required' => TRUE,
            ))
            ->add('year', null, array(
                'attr' => array(
                    'class' => 'col-lg-8',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-4',
                ),
                'required' => TRUE,
            ))
            ->add('city', null, array(
                'attr' => array(
                    'class' => 'col-lg-8',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-4',
                ),
                'required' => FALSE,
            ))
            ->add('ageAuthor', null, array(
                'attr' => array(
                    'class' => 'col-lg-8',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-4',
                ),
                'required' => FALSE,
            ));
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Books\MainBundle\Entity\FirstPublishing'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'books_firstpublishingbundle_firstpublishing';
    }
}
