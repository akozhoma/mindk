<?php

namespace Books\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditSecondPublishingType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'attr' => array(
                    'class' => 'col-lg-8',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-4',
                ),
                'required' => TRUE,
            ))
            ->add('author', null, array(
                'attr' => array(
                    'class' => 'col-lg-8',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-4',
                ),
                'required' => TRUE,
            ))
            ->add('year', null, array(
                'attr' => array(
                    'class' => 'col-lg-8',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-4',
                ),
                'required' => TRUE,
            ))
            ->add('countPages', null, array(
                'attr' => array(
                    'class' => 'col-lg-8',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-4',
                ),
                'required' => FALSE,
                'label' => 'Count of page',
            ))
            ->add('email', 'email', array(
                'attr' => array(
                    'class' => 'col-lg-8',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-4',
                ),
                'required' => FALSE,
            ))
            ->add('telephone', null, array(
                'attr' => array(
                    'class' => 'col-lg-8',
                ),
                'label_attr' => array(
                    'class' => 'col-lg-4',
                ),
                'required' => FALSE,
                'label' => 'Phone number',
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Books\MainBundle\Entity\SecondPublishing'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'books_secondpublishingbundle_secondpublishing';
    }
}
