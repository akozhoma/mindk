<?php

namespace Books\MainBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Books\MainBundle\Entity\Publishing;

class PublishingCreateListener {

    public function preAdd(LifecycleEventArgs $args) {}

    public function postAdd(LifecycleEventArgs $args) {}

    public function preRemove(LifecycleEventArgs $args) {}

    public function postRemove(LifecycleEventArgs $args) {}
}
