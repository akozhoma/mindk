<?php

namespace Books\MainBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Books\MainBundle\Entity\Publishing;
use Books\MainBundle\Form\PublishingType;
use Books\MainBundle\Entity\FirstPublishing;
use Books\MainBundle\Entity\SecondPublishing;
use Books\MainBundle\Form\EditFirstPublishingType;
use Books\MainBundle\Form\EditSecondPublishingType;

/**
 * Publishing controller.
 *
 * @Route("/book")
 */
class PublishingController extends Controller
{
    /**
     * Displays a form to create a new Publishing entity.
     *
     * @Route("/new", name="book_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Publishing();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Publishing entity.
    *
    * @param Publishing $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Publishing $entity)
    {
        $choices = array();

        // got all enabled bundles
        $bundles = $this->container->getParameter('kernel.bundles');

        // check inclusion Publishing bundles
        if (isset($bundles["BooksFirstPublishingBundle"]))
            $choices['first'] = "First publishing";

        if (isset($bundles["BooksSecondPublishingBundle"]))
            $choices['second'] = "Second publishing";

        $form = $this->createForm(new PublishingType($choices), $entity, array(
            'action' => $this->generateUrl('book_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Create new book',
            'attr'  => array(
                'class' => 'btn btn-success',
            ),
        ));

        return $form;
    }

    /**
     * Creates a new Publishing entity.
     *
     * @Route("/create", name="book_create")
     * @Method("POST")
     * @Template("BooksMainBundle:Publishing:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $chosenPublishing = NULL;

        // got all form data for new Publishing entity
        $requestData = $request->request->get("books_mainbundle_publishing");

        // got selected Publishing (first or second)
        if (isset($requestData['choicePublishing']))
            $chosenPublishing = $requestData['choicePublishing'];

        // check which Publishing is created and set needed parameters
        switch ($chosenPublishing) {
            case 'first':
                $entity = new FirstPublishing();
                $publishingType = 'firstPublishing';
                $redirectUrl = "book_with_publishing_show";

                if (!empty($requestData['firstPublishing']['city']))
                    $entity->setCity($requestData['firstPublishing']['city']);

                if (!empty($requestData['firstPublishing']['ageAuthor']))
                    $entity->setAgeAuthor($requestData['firstPublishing']['ageAuthor']);

                break;

            case 'second':
                $entity = new SecondPublishing();
                $publishingType = 'secondPublishing';
                $redirectUrl = "book_with_publishing_show";

                if (!empty($requestData['secondPublishing']['countPages']))
                    $entity->setCountPages($requestData['secondPublishing']['countPages']);

                if (!empty($requestData['secondPublishing']['email']))
                    $entity->setEmail($requestData['secondPublishing']['email']);

                if (!empty($requestData['secondPublishing']['telephone']))
                    $entity->setTelephone($requestData['secondPublishing']['telephone']);

                break;

            default:
                $entity = new Publishing();
                $publishingType = NULL;
                $entity->setPublishingType("0");
                $redirectUrl = "book_show";
                break;
        }

        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl($redirectUrl, array(
                'id' => $entity->getId(),
                'type' => $publishingType,
            )));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Publishing entity.
     *
     * @Route("/{id}/show", name="book_show")
     * @Route("/{type}/{id}/show", name="book_with_publishing_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id, $type = NULL)
    {
        $em = $this->getDoctrine()->getManager();

        // check which one Publishing should be show and load needed Publishing entity
        if ($type == "firstPublishing") {
            $entity = $em->getRepository('BooksMainBundle:FirstPublishing')->find($id);
            $publishingName = "First publishing";
        } elseif ($type == "secondPublishing") {
            $entity = $em->getRepository('BooksMainBundle:SecondPublishing')->find($id);
            $publishingName = "Second publishing";
        } else {
            $entity = $em->getRepository('BooksMainBundle:Publishing')->find($id);
        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Publishing entity.');
        }

        $editUrl = "book_edit";
        if (isset($type))
            $editUrl = "book_with_publishing_edit";

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'type'        => $type,
            'publishing'  => isset($publishingName) ? $publishingName : '',
            'edit_url'    => $editUrl,
        );
    }

    /**
     * Displays a form to edit an existing Publishing entity.
     *
     * @Route("/{id}/edit", name="book_edit")
     * @Route("/{type}/{id}/edit", name="book_with_publishing_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id, $type = NULL)
    {
        $em = $this->getDoctrine()->getManager();

        // check which one Publishing should be edit and load needed Publishing entity
        if ($type == "firstPublishing") {
            $entity = $em->getRepository('BooksMainBundle:FirstPublishing')->find($id);
        } elseif ($type == "secondPublishing") {
            $entity = $em->getRepository('BooksMainBundle:SecondPublishing')->find($id);
        } else {
            $entity = $em->getRepository('BooksMainBundle:Publishing')->find($id);
        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Publishing entity.');
        }

        $editForm = $this->createEditForm($entity, $type);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'type'        => $type,
        );
    }

    /**
    * Creates a form to edit a Publishing entity.
    *
    * @param Publishing $entity The entity
    * @param string $type Type of Publishing entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm($entity, $type)
    {
        $redirectUrl = "book_update";

        // check which one Publishing should be update, set needed parameters and load needed form for edit
        if ($type == "firstPublishing") {
            $preferredChoice = "first";
            $editForm = new EditFirstPublishingType();
            $redirectUrl = "book_with_publishing_update";
        } elseif ($type == "secondPublishing") {
            $preferredChoice = "second";
            $editForm = new EditSecondPublishingType();
            $redirectUrl = "book_with_publishing_update";
        } else {
            $preferredChoice = "";
            $editForm = new PublishingType();
        }

        $form = $this->createForm($editForm, $entity, array(
            'action' => $this->generateUrl($redirectUrl, array(
                'id' => $entity->getId(),
                'type' => $type,
            )),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Save changes',
            'attr'  => array(
                'class' => 'btn btn-success',
            ),
        ));

        return $form;
    }

    /**
     * Edits an existing Publishing entity.
     *
     * @Route("/{id}/update", name="book_update")
     * @Route("/{type}/{id}/update", name="book_with_publishing_update")
     * @Method("PUT")
     * @Template("BooksMainBundle:Publishing:edit.html.twig")
     */
    public function updateAction(Request $request, $id, $type = NULL)
    {
        $em = $this->getDoctrine()->getManager();

        $redirectUrl = "book_show";

        // check which one Publishing should be update, set needed parameters and load needed Publishing entity
        if (isset($type) && $type == 'firstPublishing') {
            $entity = $em->getRepository('BooksMainBundle:FirstPublishing')->find($id);
            $redirectUrl = "book_with_publishing_show";
        } elseif (isset($type) && $type == 'secondPublishing') {
            $entity = $em->getRepository('BooksMainBundle:SecondPublishing')->find($id);
            $redirectUrl = "book_with_publishing_show";
        } else {
            $entity = $em->getRepository('BooksMainBundle:Publishing')->find($id);
        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Publishing entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity, $type);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl($redirectUrl, array(
                'id' => $id,
                'type' => $type,
            )));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            '$type'       => $type,
        );
    }

    /**
     * Deletes a Publishing entity.
     *
     * @Route("/{id}/delete", name="book_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BooksMainBundle:Publishing')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Publishing entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('homepage'));
    }

    /**
     * Creates a form to delete a Publishing entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(null, array('csrf_protection' => false))
            ->setAction($this->generateUrl('book_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array(
                'label' => 'Delete',
                'attr'  => array(
                    'class' => 'btn btn-danger col-lg-offset-2',
                ),
            ))
            ->getForm()
        ;
    }
}
