<?php

namespace Books\FirstPublishingBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Books\MainBundle\Entity\FirstPublishing;
use Symfony\Component\Config\Definition\Exception\Exception;

class PublishingCreateSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            'preRemove',
        );
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $this->index($args);
    }

    public function index(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof FirstPublishing) {
            throw new Exception('Deleting book of this publishing is unpossible');
        }
    }
}
